package com.powerflasher.SampleApp {
	import de.polygonal.ds.GraphNode;
	import de.polygonal.ds.Graph;
	import flash.display.Sprite;

	public class polygonalds extends Sprite {
		public function polygonalds() {
			
			var graph:Graph = new Graph();		
			var node1:GraphNode = graph.createNode( "node1" );
			var node2:GraphNode = graph.createNode( "node2" );
			var node3:GraphNode = graph.createNode( "node3" );
			var node4:GraphNode = graph.createNode( "node4" );
			
			graph.addNode( node1 );
			graph.addNode( node2 );
			graph.addNode( node3 );
			graph.addNode( node4 );
			
			graph.addMutualArc(node1, node2);
			graph.addSingleArc(node1, node3);
			graph.addSingleArc(node3, node2);
			graph.addMutualArc(node3, node4);
			
			var preflight:Boolean = false;
			var seed:GraphNode = graph.getNodeList();
			
//			trace( "graph.size:", graph.size() );
			trace( "seed:", seed.val );
			
			graph.BFS(preflight, seed, _processNode, null);
		}
		
		private function _processNode(node:GraphNode, preflight:Boolean, userData:Dynamic) : Boolean {
			trace( node.val );
			return true; 
		}
	}
	
}
